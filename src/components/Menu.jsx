import React, { useState } from 'react'
import { Navbar, Nav, Form, Button, FormControl, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Menu() {
  const [id,setId] = useState(' ');
  
  return (
    <>
      <Container className="mt-1">
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home" >React-Router</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="justify-content-between">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <Nav.Link as={Link} to="/video">Video</Nav.Link>
              <Nav.Link as={Link} to={`/account/${id}`}>Account</Nav.Link>
              <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
              <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
              <Nav.Link as={Link} to={`/Detail/${id}`}></Nav.Link>

            </Nav>
            <Form className="d-flex flex-row">
              <FormControl type="text" placeholder="Search" className="mr-sm-2" />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </Container>

    </>

  )
}
