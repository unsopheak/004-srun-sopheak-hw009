import React,{Components} from 'react'
import {Redirect} from 'react-router'
export default function ProtectedRoute({isLogin,component:Component,path}) {
   if(isLogin){
        return <Component path={path}/>
   }else{
       return <Redirect to="/welcome" />
   }
}