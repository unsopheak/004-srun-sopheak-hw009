import React from 'react'
import { Container,Form,Button, Col } from 'react-bootstrap'



export default function Welcome(props) {
  
 
   return (
      <Container >
         <h1>Login</h1>
         <Col sm={6}>
         <Form>
  <Form.Group controlId="formBasicEmail" >
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" />
    <Form.Text className="text-muted">
      Please input your email!
    </Form.Text>
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>
  <Form.Group controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Check me out" />
  </Form.Group>
  <Button onClick={props.onLogin} variant="primary">
  {props.isLogin?"Logout": "Login"}
  </Button>
</Form>
</Col>
      </Container>
    )
}
