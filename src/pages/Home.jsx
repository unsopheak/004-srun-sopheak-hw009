import { Card, Button, Container, Row, Col } from 'react-bootstrap'
import React from 'react'
import { Link, useParams } from 'react-router-dom';


export default function Home() {
  let { id } = useParams();

  return (
    <Container className="mt-2">
      <Row>
        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://lumiere-a.akamaihd.net/v1/images/cg_horizontal_snowwhite_story_mobile_20694_0fa99d84.jpeg?region=0%2C0%2C1024%2C768" />
          <Card.Body>
            <Card.Title>Snow White</Card.Title>
            <Card.Text>
              Snow White knows that every day is an opportunity to make new friends, and that it’s best to be kind to everyone, even if they’re grumpy!
    </Card.Text>

            <Link to={`/detail/${1}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>
        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://cdn.britannica.com/28/144428-050-B9F43ED1/Snow-White-and-the-Seven-Dwarfs.jpg" />
          <Card.Body>
            <Card.Title>The Seven Dwarfs</Card.Title>
            <Card.Text>
              American animated musical film released in 1937, that established Walt Disney as one of the world’s most innovative and creative moviemakers. Along with Pinocchio (1940)
    </Card.Text>
            <Link to={`/detail/${2}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>
        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://sir.wdwnt.com/wdwnt.com/2020/03/snow-white-and-the-seven-dwarfs-1937.jpg" />
          <Card.Body>
            <Card.Title>Disenchanted</Card.Title>
            <Card.Text>
              Last month, it was revealed that Disenchanted, the much-anticipated sequel to 2007’s Enchanted, will have its full starring cast confirmed to return.
    </Card.Text>
            <Link to={`/detail/${3}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>
        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://i2-prod.mirror.co.uk/incoming/article13839132.ece/ALTERNATES/s1200b/0_Disney.jpg" />
          <Card.Body>
            <Card.Title>Sleeping Beauty</Card.Title>
            <Card.Text>
              A pink wonderland for the child’s room! Sleeping Beauty and her white horse are waiting for prince charming in front of her castle.Welcome to Moonlight Studio Store.
    </Card.Text>
            <Link to={`/detail/${4}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>
      </Row>

      <Row className="mt-2">
        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://lumiere-a.akamaihd.net/v1/images/cg_horizontal_snowwhite_story_mobile_20694_0fa99d84.jpeg?region=0%2C0%2C1024%2C768" />
          <Card.Body>
            <Card.Title>Snow White</Card.Title>
            <Card.Text>
              Snow White knows that every day is an opportunity to make new friends, and that it’s best to be kind to everyone, even if they’re grumpy!
    </Card.Text>
            <Link to={`/detail/${5}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>

        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://lumiere-a.akamaihd.net/v1/images/cg_horizontal_snowwhite_story_mobile_20694_0fa99d84.jpeg?region=0%2C0%2C1024%2C768" />
          <Card.Body>
            <Card.Title>Snow White</Card.Title>
            <Card.Text>
              Snow White knows that every day is an opportunity to make new friends, and that it’s best to be kind to everyone, even if they’re grumpy!
    </Card.Text>
            <Link to={`/detail/${6}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>

        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://lumiere-a.akamaihd.net/v1/images/cg_horizontal_snowwhite_story_mobile_20694_0fa99d84.jpeg?region=0%2C0%2C1024%2C768" />
          <Card.Body>
            <Card.Title>Snow White</Card.Title>
            <Card.Text>
              Snow White knows that every day is an opportunity to make new friends, and that it’s best to be kind to everyone, even if they’re grumpy!
    </Card.Text>
            <Link to={`/detail/${7}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>
        <Col sm={12} md={6} lg={4} xl={3}><Card >
          <Card.Img className="Card" variant="top" src="https://lumiere-a.akamaihd.net/v1/images/cg_horizontal_snowwhite_story_mobile_20694_0fa99d84.jpeg?region=0%2C0%2C1024%2C768" />
          <Card.Body>
            <Card.Title>Snow White</Card.Title>
            <Card.Text>
              Snow White knows that every day is an opportunity to make new friends, and that it’s best to be kind to everyone, even if they’re grumpy!
    </Card.Text>
            <Link to={`/detail/${8}`}>
              <Button variant="info">Read</Button>
            </Link>
          </Card.Body>
        </Card></Col>
        
        
      </Row>
    </Container>

  )
}
