import React,{useState}from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
} from "react-router-dom";
import queryString from 'query-string'
import { Card, Button, Row, Container,ButtonGroup } from "react-bootstrap";
export default function Nested() {
  return (
    
   
 
    <Router> 
        
      <Container>
        <h1>Video</h1>
       <ButtonGroup>
       <Button variant="secondary">
        <Link to="/movie">Movie</Link>
        </Button>
        <Button variant="secondary">
          <Link to="/animation">Animation</Link>
        </Button>
        </ButtonGroup>
        <Switch>
        
          <Route path="/animation">
            <Animation />
          </Route>

          <Route path="/movie">
            <Movie />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}



function Animation() {
    const { path, url } = useRouteMatch();
    const[name,setName]=useState("nita")
    return (
      <div>
        <h2>Movies Category</h2>
        <div
          class="btn-group"
          role="group"
          aria-label="Basic mixed styles example"
        >
          <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Action`}>
              Action
            </Link>
          </button>
          {/* ?name=? */}
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Romance`}>Romance</Link>
          </button>
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Comedy`}>Comedy</Link>
          </button>
        </div>
  
        <Switch>
          <Route path={`${path}/:animationId`}>
            <Animations />
          </Route>
        </Switch>
      </div>
    );
}

function Animations() {
  let { animationId } = useParams();
  return (
    <div>
      <h3>
        Please Choose Category:
        <span className="animationId">{animationId}</span>
      </h3>
    </div>
  ); }
  
  
  function Movie() {
    let { path, url } = useRouteMatch();
  
    return (
      <Container>
        <h2>Movies Category</h2>
        <div
          class="btn-group"
          role="group"
          aria-label="Basic mixed styles example"
        >
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Advanture`}>
              Advanture
            </Link>
          </button>
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Crime`}>Crime</Link>
          </button>
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Action`}>Action</Link>
          </button>
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Romance`}>
              Romance
            </Link>
          </button>
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Romance`}>Romance</Link>
          </button>
          <button type="button" class="btn btn-secondary">
            <Link to={`${url}/Comedy`}>Comedy</Link>
          </button>
        </div>
  
        <Switch>
          <Route path={`${path}/:animation1Id`}>
            <Animationss />
          </Route>
        </Switch>
      </Container>
    );
  }

function Animationss() {

      let { animation1Id } = useParams();
  return (
    <div>
      <h3>
        Please Choose Category:<span className="animationId">{animation1Id}</span>
      </h3>
    </div>
  )}