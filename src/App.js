import "bootstrap/dist/css/bootstrap.min.css";
import React,{useState} from "react";
import ReactDOM from 'react-dom';
import './App.css';
import Menu from "./components/Menu";
import Account from "./pages/Account";
import Auth from "./pages/Auth";
import Video from "./pages/Video";
import Welcome from "./pages/Welcome";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Detail from "./pages/Detail";
import ProtectedRoute from "./pages/ProtectedRoute";


export default function App() {

  const [isLogin, setLogin]= useState(false)
  
  function onLogin(){
    setLogin(!isLogin)
  }  

  return (
    <Router>
      <Menu />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/detail/:id" component={Detail} />
        <Route path="/video" component={Video} />
        <Route path="/account/:id" component={Account} />
        {/* <Route path="/welcome" component={Welcome} /> */}
        <Route render={()=><Welcome onSignin={onLogin} isSignin={isLogin}/>}/>
        <ProtectedRoute isLogin={isLogin} component={Auth} path="/auth"/>
        <Route path="/:id" children={<Account />} />
      </Switch>
    </Router>
  );
}
